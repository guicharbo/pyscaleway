
# 200 OK - Everything worked as expected.
#
# 400 Bad Request - Often missing a required parameter.
#
# 401 Unauthorized - No valid API key provided.
#
# 402 Request Failed - Parameters were valid but request failed.
#
# 403 Forbidden - Insufficient privileges to access requested resource.
#
# 404 Not Found - The requested item doesn’t exist.
#
# 50x Server errors - something went wrong on api domain’s end.

def is_ok(code):
    return 200 <= code < 300


def is_bad(code):
    return code == 400


def is_unauthorized(code):
    return code == 401


def has_failed(code):
    return code == 402


def is_forbidden(code):
    return code == 403


def is_not_found(code):
    return code == 404


def is_server_error(code):
    return 500 <= code < 600